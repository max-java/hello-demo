package com.max;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( hello("World") );
    }

    public static String hello(String name){
        return "Hello "+ name;
    }
}
