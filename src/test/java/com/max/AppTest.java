package com.max;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void helloTest()
    {
        assertThat(App.hello("World"))
                .isEqualTo("Hello World");
    }
}
